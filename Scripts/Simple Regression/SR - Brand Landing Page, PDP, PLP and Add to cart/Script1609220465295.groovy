import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//WebUI.openBrowser('')
'Go to brand listing page'
WebUI.navigateToUrl(GlobalVariable.brandPage_URL)

'click adidas'
WebUI.click(findTestObject('BrandListingPage/brandPage_ADIDAS'))

'check heading'
WebUI.verifyElementText(findTestObject('BrandLandingPage_ADIDAS/brandLandingPage_ADIDAS_Heading'), 
    'ADIDAS', FailureHandling.CONTINUE_ON_FAILURE)

'check banner'
WebUI.verifyElementVisible(findTestObject('BrandLandingPage_ADIDAS/brandLandingPage_ADIDAS_Banner'), 
    FailureHandling.CONTINUE_ON_FAILURE)

'check filter bar'
WebUI.verifyElementVisible(findTestObject('BrandLandingPage_ADIDAS/brandLandingPage_ADIDAS_PLPFilter'), 
    FailureHandling.CONTINUE_ON_FAILURE)

'check PLP list'
WebUI.verifyElementVisible(findTestObject('BrandLandingPage_ADIDAS/brandLandingPage_ADIDAS_PLP'), 
    FailureHandling.CONTINUE_ON_FAILURE)

'check category filter on the left'
WebUI.verifyElementVisible(findTestObject('BrandLandingPage_ADIDAS/brandLandingPage_ADIDAS_CategoryFilter'), 
    FailureHandling.CONTINUE_ON_FAILURE)

'check default sorting is 暢銷商品'
WebUI.verifyElementVisible(findTestObject('BrandLandingPage_ADIDAS/brandLandingPage_ADIDAS_PLPFilterDefaultSorting'), 
    FailureHandling.CONTINUE_ON_FAILURE)

'go to pdp'
WebUI.click(findTestObject('BrandLandingPage_ADIDAS/brandLandingPage_ADIDAS_PLP_Product1'))

'check pdp heading'
WebUI.verifyElementVisible(findTestObject('PDP_ADIDAS_Product1/PDP_ADIDAS_Product1_Heading'), FailureHandling.CONTINUE_ON_FAILURE)

'check buy bar'
WebUI.verifyElementVisible(findTestObject('PDP_ADIDAS_Product1/PDP_ADIDAS_Product1_buyBar'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('PDP_ADIDAS_Product1/PDP_ADIDAS_Product1_AddToWishListBtn'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('PDP_ADIDAS_Product1/PDP_ADIDAS_Product1_OthersBoughtSection'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('PDP_ADIDAS_Product1/PDP_ADIDAS_Product1_DescSection'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('PDP_ADIDAS_Product1/PDP_ADIDAS_Product1_PromotionSection'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('PDP_ADIDAS_Product1/PDP_ADIDAS_Product1_PaymentDeliverySection'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('PDP_ADIDAS_Product1/PDP_ADIDAS_Product1_CommentSection'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('PDP_ADIDAS_Product1/PDP_ADIDAS_Product1_YouMayAlsoLikeSection'), 
    FailureHandling.CONTINUE_ON_FAILURE)

'add 2 to qty'
WebUI.click(findTestObject('PDP_ADIDAS_Product1/PDP_ADIDAS_Product1_AddQtyBtn'))

WebUI.click(findTestObject('PDP_ADIDAS_Product1/PDP_ADIDAS_Product1_AddQtyBtn'))

'remove 1 qty'
WebUI.click(findTestObject('PDP_ADIDAS_Product1/PDP_ADIDAS_Product1_RmQtyBtn'))

'add to cart'
WebUI.click(findTestObject('PDP_ADIDAS_Product1/PDP_ADIDAS_Product1_AddToBasketBtn'))


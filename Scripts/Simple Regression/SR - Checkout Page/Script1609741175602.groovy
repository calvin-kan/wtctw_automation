import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Generate random number'
String randomNumber = '09' + String.valueOf(Math.abs(new Random().nextInt() % (99999999 - 11111111)) + 11111111)

'check elements visible'
WebUI.verifyElementVisible(findTestObject('Checkout/checkout_deliveryInfoSection'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('Checkout/checkout_deliveryOptionsSection'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('Checkout/checkout_codeVoucherSection'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('Checkout/Payment Method/checkout_paymentMethodSection'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('Checkout/checkout_PayBtn'), FailureHandling.CONTINUE_ON_FAILURE)

'fill in the name of new address'
WebUI.setText(findTestObject('Checkout/HD - New Address/checkout_newAddress_NameField'), 'address new')

'fill in the mobile of new address with random number'
WebUI.setText(findTestObject('Checkout/HD - New Address/checkout_newAddress_MobileField'), randomNumber)

'open the city selection dropdown'
WebUI.click(findTestObject('Checkout/HD - New Address/checkout_newAddress_CitySelectionDropdownBtn'))

WebUI.click(findTestObject('Checkout/HD - New Address/checkout_newAddress_CitySelection'))

'select a city'
WebUI.selectOptionByValue(findTestObject('Checkout/HD - New Address/checkout_newAddress_CitySelection_Citry1'), 'TW_320', 
    true)

'open the district selection dropdown'
WebUI.click(findTestObject('Checkout/HD - New Address/checkout_newAddress_DistrictSelectionDropdownBtn'))

WebUI.click(findTestObject('Checkout/HD - New Address/checkout_newAddress_DistrictSelection'))

'select a district'
WebUI.selectOptionByValue(findTestObject('Checkout/HD - New Address/checkout_newAddress_DistrictSelection_District1'), 
    '327', true)

'fill in the street name'
WebUI.setText(findTestObject('Checkout/HD - New Address/checkout_newAddress_StreetNameField'), 'new address')

'enable checkbox "add to address book"'
WebUI.click(findTestObject('Checkout/HD - New Address/checkout_newAddress_AddToAddressBkChkbox'))

'save and delivery to this address'
WebUI.click(findTestObject('Checkout/HD - New Address/checkout_newAddress_SaveAndDelToThisAddressBtn'))

'verify address has been added'
WebUI.verifyTextPresent('address new', false)

WebUI.verifyTextPresent(randomNumber, false)

WebUI.verifyTextPresent('new address', false)


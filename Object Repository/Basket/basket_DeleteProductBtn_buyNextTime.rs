<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>basket_DeleteProductBtn_buyNextTime</name>
   <tag></tag>
   <elementGuidId>0da5d595-6bbf-41e3-8619-e9f5a1d20eae</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@class='myBasketContent part-next-time']/*/*/*/div[@class='td productRemove width10']/i</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>

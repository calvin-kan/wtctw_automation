<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>checkout_PickPayBtn</name>
   <tag></tag>
   <elementGuidId>8fe2b865-1225-42a2-aac2-409ac1e8fcc5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@class='select-method tabPaymentKiosk btn btn-paymentMethod']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.select-method.tabPaymentKiosk.btn.btn-paymentMethod</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>select-method tabPaymentKiosk btn btn-paymentMethod</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    
                        超商取貨付款
                            (只限台灣地區用戶)
                        
                    </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mainContent&quot;)/main[1]/div[@class=&quot;wrapper&quot;]/div[@class=&quot;container-left&quot;]/section[@class=&quot;container-checkout-paymentMethod&quot;]/div[@class=&quot;detail&quot;]/div[@class=&quot;select-payment-method checkout-paymentMethod&quot;]/div[@class=&quot;select-method tabPaymentKiosk btn btn-paymentMethod&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='mainContent']/main/div/div/section[4]/div[2]/div/div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='(只限台灣地區用戶)'])[1]/following::div[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='超商取貨付款']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section[4]/div[2]/div/div[3]</value>
   </webElementXpaths>
</WebElementEntity>

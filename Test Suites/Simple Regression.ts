<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Simple Regression</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>c7c76a04-5ba2-4697-8c87-171e0c628cb5</testSuiteGuid>
   <testCaseLink>
      <guid>c744d894-495b-4058-beb8-526afd239f60</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Simple Regression/SR - Homepage</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d183c58f-17b8-4586-b258-597856c59bac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Simple Regression/SR - Store Locator</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>2c70f3f1-8560-42e9-a61b-cc8f95809970</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>2fc91537-2e28-4553-9efd-adbdf2eafb91</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>2af39cbb-dc78-4fed-93b8-f35737c51ade</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Simple Regression/SR - Brand Page All brand links</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d6f0d655-d3b9-41fe-81af-fba04eab5816</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Simple Regression/SR - Search</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d49df3d3-ff8a-42a0-b718-f34174321702</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>8e61a388-02d5-4e31-a4fb-ac9db44ff467</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Simple Regression/SR - Category Landing Page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>22387d14-033d-43e7-b995-a424bce1b25d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Simple Regression/SR - Member Login and Profile Update</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>fd10bb0f-35a6-4176-a659-0433d02c604f</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>446aac6c-d73b-42da-bab9-a34e2175361d</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>aa3b2b5d-fe15-4f86-98a3-fd941a1cd547</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Simple Regression/SR - eShopper Registration</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cc6aecff-a459-482d-8016-ac462dc5c311</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Simple Regression/SR - Member Registration and Address Book</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fd80383c-815a-45ea-a378-44c7c40d1d0f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Simple Regression/SR - Brand Landing Page, PDP, PLP and Add to cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3cc42607-5851-4b07-8ea5-88e20477f3da</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Simple Regression/SR - Basket</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d694b43f-453a-44dc-b2cc-49578d94792d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Simple Regression/SR - Checkout Page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9c8b1d05-01ff-4684-852b-ef777507f29c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Simple Regression/SR - Change and reset PW</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ba288a59-b838-4b9b-8af4-6b0a70398321</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Simple Regression/SR - Buy Later</testCaseId>
   </testCaseLink>
</TestSuiteEntity>

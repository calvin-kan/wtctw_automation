import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.util.concurrent.ThreadLocalRandom as Keyword

'Generate random number'
String randomNumber = String.valueOf(Math.abs(new Random().nextInt() % (99999999 - 11111111)) + 11111111)

'Declare strings'
String memberEmail = 'autotesting-' + randomNumber + GlobalVariable.BU + '@yopmail.com'
String memberName = 'Auto Testing ' + randomNumber + ' member'
String memberMobile = '09' + randomNumber
String memberAddressName = 'Auto Testing' + randomNumber + 'address 1'


WebUI.openBrowser('')

'Go to WTCTW UAT'
WebUI.navigateToUrl(GlobalVariable.defaultURL)

'Go to my account'
WebUI.enhancedClick(findTestObject('Home/home_MyAccountBtn'))

//Registration

'enhancedClick register button'
WebUI.enhancedClick(findTestObject('Login/login_RegisterButton'))

'Enter name with randomly generated number'
WebUI.setText(findTestObject('Registration/registration_NameField'), memberName)

'Paste email'
WebUI.sendKeys(findTestObject('Registration/registraion_EmailField'), memberEmail)

'Paste email again'
WebUI.sendKeys(findTestObject('Registration/registration_EmailVerificationField'), memberEmail)

'Enter TW mobile'
WebUI.sendKeys(findTestObject('Registration/registration_MobileField'), memberMobile)

'Enter pw'
WebUI.sendKeys(findTestObject('Registration/registration_PasswordField'), GlobalVariable.defaultPassword)

'Enter pw again'
WebUI.sendKeys(findTestObject('Registration/registration_PwVerificationField'), GlobalVariable.defaultPassword)

'submit'
WebUI.enhancedClick(findTestObject('Registration/registration_SubmitButton'))

'enter correct otp'
WebUI.setText(findTestObject('OTP/otp_otpField'), GlobalVariable.defaultOTP)

'submit otp'
WebUI.enhancedClick(findTestObject('OTP/otp_confirmOTPButton'))

'check registration successful'
WebUI.verifyTextPresent(GlobalVariable.reg_Success_Welcome, false)

WebUI.verifyTextPresent(GlobalVariable.reg_Success_MemberText, false)

'Print email and pw to log'
println('\n\n####\nEmail : '+ memberEmail + '\nPassword : ' + GlobalVariable.defaultPassword + '\n####')

//Address Book

WebUI.enhancedClick(findTestObject('Home/home_MyAccountBtn'))

'enhancedClick the address book link'
WebUI.enhancedClick(findTestObject('AccountSummary/account_AddressBook'))

'add address'
WebUI.enhancedClick(findTestObject('AddressBook/addressBook_AddAddress'))

'Enter name with randomly generated number'
WebUI.setText(findTestObject('AddressBook/addressBook_NameField'), memberAddressName)

'Enter TW mobile'
WebUI.setText(findTestObject('AddressBook/addressBook_MobileField'), memberMobile)

WebUI.enhancedClick(findTestObject('AddressBook/addressBook_CityDropdownBtn'))

WebUI.enhancedClick(findTestObject('AddressBook/addressBook_CityDropdown'))

WebUI.selectOptionByValue(findTestObject('AddressBook/addressBook_City1'), 'TW_200', true)

WebUI.enhancedClick(findTestObject('AddressBook/addressBook_DistrictDropdownBtn'))

WebUI.enhancedClick(findTestObject('AddressBook/addressBook_DistrictDropdown'))

WebUI.selectOptionByValue(findTestObject('AddressBook/addressBook_District1'), '201', true)

WebUI.setText(findTestObject('AddressBook/addressBook_StreetNameField'), 'address')

WebUI.enhancedClick(findTestObject('AddressBook/addressBook_UpdateAddressBtn'))

'check the name was displayed correctly'
WebUI.verifyTextPresent(memberAddressName, false)

'check the mobile is displayed correctly'
WebUI.verifyTextPresent(memberMobile, false)

'edit address'
WebUI.enhancedClick(findTestObject('AddressBook/addressBook_EditAddressBtn'))

WebUI.setText(findTestObject('AddressBook/addressBook_StreetNameField'), 'address edited')

'update address'
WebUI.enhancedClick(findTestObject('AddressBook/addressBook_UpdateAddressBtn'))

WebUI.verifyTextPresent('address edited', false)

'delete address'
WebUI.enhancedClick(findTestObject('AddressBook/addressBook_DeleteAddressBtn'))

WebUI.verifyTextNotPresent('address edited', false)


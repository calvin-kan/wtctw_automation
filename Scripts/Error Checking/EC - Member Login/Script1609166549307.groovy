import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

'Go to CS cockpit'
WebUI.navigateToUrl(GlobalVariable.csURL)

'login to cscockpit'
WebUI.setText(findTestObject('CSCockpit/cscockpit_Username'), GlobalVariable.csLogin)

'enter cs pw'
WebUI.setText(findTestObject('CSCockpit/cscockpit_Password'), GlobalVariable.csPassword)

'press enter'
WebUI.sendKeys(findTestObject('CSCockpit/cscockpit_Password'), Keys.chord(Keys.ENTER))

'click find customer'
WebUI.click(findTestObject('CSCockpit/cscockpit_Find Customer'))

'enter autotesting email'
WebUI.setText(findTestObject('CSCockpit/cscockpit_FindCus_EmailField'), GlobalVariable.defaultLogin)

'click search button'
WebUI.click(findTestObject('CSCockpit/cscockpit_FindCus_SearchButton'))

'click select button'
WebUI.click(findTestObject('CSCockpit/cscockpit_FindCus_SelectButton'))

'reset login fail email count'
WebUI.click(findTestObject('CSCockpit/cscockpit_ResetLoginFailCount'))

'go to homepage'
WebUI.navigateToUrl(GlobalVariable.defaultURL)

WebUI.click(findTestObject('Home/home_MyAccountBtn'))

'click login to trigger error messages'
WebUI.click(findTestObject('Login/login_loginButton'))

WebUI.verifyElementText(findTestObject('Login/errors_Login_EmptyEmail'), GlobalVariable.globalError_Login_EmptyEmail)

WebUI.verifyElementText(findTestObject('Login/errors_Login_EmptyPw'), GlobalVariable.globalError_Login_EmptyPw)

WebUI.setText(findTestObject('Login/login_usernameField'), 'wrongemail')

WebUI.setText(findTestObject('Login/login__passwordField'), 'wrongpw')

'click login to trigger error messages'
WebUI.click(findTestObject('Login/login_loginButton'))

WebUI.verifyElementText(findTestObject('Login/errors_Login_AccountNotExists'), GlobalVariable.globalError_Login_AccountNotExists)

'Wrong pw message 1-6th time'
for (int wrongpwcount = 0; wrongpwcount <= 5; wrongpwcount++) {
    WebUI.setText(findTestObject('Login/login_usernameField'), GlobalVariable.defaultLogin)

    WebUI.setText(findTestObject('Login/login__passwordField'), 'wrongpw')

    WebUI.click(findTestObject('Login/login_loginButton'))

    WebUI.verifyElementText(findTestObject('Login/errors_Login_WrongPw1-6'), GlobalVariable.globalError_Login_WrongPw1_6)

    WebUI.navigateToUrl(GlobalVariable.csIndexURL)

    WebUI.delay(3)

    WebUI.back()
}

'Wrong pw message 7-10th time'
for (int wrongPwCountBeforeAcLock = 3; wrongPwCountBeforeAcLock >= -1; wrongPwCountBeforeAcLock--) {
    WebUI.setText(findTestObject('Login/login_usernameField'), GlobalVariable.defaultLogin)

    WebUI.setText(findTestObject('Login/login__passwordField'), 'wrongpw')

    WebUI.click(findTestObject('Login/login_loginButton'))

    if ((wrongPwCountBeforeAcLock <= 3) && (wrongPwCountBeforeAcLock >= 1)) {
        WebUI.verifyElementText(findTestObject('Login/errors_Login_WrongPw7-9'), ('若再輸入錯誤 ' + wrongPwCountBeforeAcLock) + 
            ' 次，帳號將被鎖定30分鐘 。建議立即點選 「忘記密碼」，重新設定新密碼。')
    } else if (wrongPwCountBeforeAcLock == 0) {
        WebUI.verifyElementText(findTestObject('Login/errors_Login_WrongPw10'), GlobalVariable.globalError_LoginWrongPw10)
    }
    
    WebUI.refresh()
}

'Go to CS cockpit'
WebUI.navigateToUrl(GlobalVariable.csURL)

'login to cscockpit'
WebUI.setText(findTestObject('CSCockpit/cscockpit_Username'), GlobalVariable.csLogin)

'enter cs pw'
WebUI.setText(findTestObject('CSCockpit/cscockpit_Password'), GlobalVariable.csPassword)

'press enter'
WebUI.sendKeys(findTestObject('CSCockpit/cscockpit_Password'), Keys.chord(Keys.ENTER))

'click find customer'
WebUI.click(findTestObject('CSCockpit/cscockpit_Find Customer'))

'enter autotesting email'
WebUI.setText(findTestObject('CSCockpit/cscockpit_FindCus_EmailField'), GlobalVariable.defaultLogin)

'click search button'
WebUI.click(findTestObject('CSCockpit/cscockpit_FindCus_SearchButton'))

'click select button'
WebUI.click(findTestObject('CSCockpit/cscockpit_FindCus_SelectButton'))

'reset login fail email count'
WebUI.click(findTestObject('CSCockpit/cscockpit_ResetLoginFailCount'))


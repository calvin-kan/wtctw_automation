import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.navigateToUrl(GlobalVariable.defaultURL)

//Delete items in shopping cart if found any
Boolean shoppingCartItemCountNotZero = !(WebUI.getText(findTestObject('GlobalObjects/basket_ItemCount')).equals('0'))

'see if there is anything in the cart, delete if any'
if (shoppingCartItemCountNotZero) {
    'go to basket'
    WebUI.enhancedClick(findTestObject('GlobalObjects/basket_Icon'))

    Boolean crossBtnFound = WebUI.verifyElementPresent(findTestObject('Basket/basket_DeleteProductBtn'), 1, FailureHandling.OPTIONAL)

    while (crossBtnFound) {
        WebUI.enhancedClick(findTestObject('Basket/basket_DeleteProductBtn'), FailureHandling.CONTINUE_ON_FAILURE)

        crossBtnFound = WebUI.verifyElementPresent(findTestObject('Basket/basket_DeleteProductBtn'), 1, FailureHandling.OPTIONAL)
    }
}

///
'mouse over the my account btn'
WebUI.mouseOver(findTestObject('Home/home_MyAccountBtn'))

'go to shopping list'
WebUI.enhancedClick(findTestObject('Home/home_shoppingList'))

//make sure the default shopping list is selected
WebUI.enhancedClick(findTestObject('ShoppingList/shoppingList_dropdownArrow'))

WebUI.enhancedClick(findTestObject('ShoppingList/shoppingList_regressionTest'))

///
'Add list twice'
for (int addToBasketCount = 1; addToBasketCount <= 2; addToBasketCount++) {
    WebUI.enhancedClick(findTestObject('ShoppingList/shoppingList_AddAllToBasketBtn'))

    WebUI.enhancedClick(findTestObject('ShoppingList/shoppingList_RemoveAllFromListPopUp_cancelBtn'))
}

'go to basket'
WebUI.enhancedClick(findTestObject('GlobalObjects/basket_Icon'))

WebUI.focus(findTestObject('Basket/Delivery Method/basket_HDchkbox'))

'select HD'
WebUI.enhancedClick(findTestObject('Basket/Delivery Method/basket_HDchkbox'))

'checkout'
WebUI.enhancedClick(findTestObject('Basket/basket_leftCheckoutBtn'))

'redeem points if point redemption test is required'
if (GlobalVariable.pointRedemptionTest) {
    WebUI.mouseOver(findTestObject('Checkout/checkout_eWallet'))

    'open ewallet'
    WebUI.enhancedClick(findTestObject('Checkout/checkout_eWallet'))

    WebUI.setText(findTestObject('Checkout/checkout_pointRedemptionField'), '1')

    WebUI.enhancedClick(findTestObject('Checkout/checkout_pointRedemptionbtn'))
}

'select payment method'
WebUI.enhancedClick(findTestObject('Checkout/Payment Method/checkout_CreditCardOneTimePaymentBtn'))

WebUI.enhancedClick(findTestObject('Checkout/Payment Method/checkout_CathayBankCardRadioBtn'))

'pay'
WebUI.enhancedClick(findTestObject('Checkout/checkout_PayBtn'))

'enter credit card credentials'
WebUI.setText(findTestObject('PaymentGateway/CathayBank/paymentGateway_CathayBank_cardNoField'), GlobalVariable.cathayBankCreditCardNo)

WebUI.selectOptionByValue(findTestObject('PaymentGateway/CathayBank/paymentGateway_CathayBank_monthField'), GlobalVariable.cathayBankCreditCardMonth, 
    false)

WebUI.selectOptionByValue(findTestObject('PaymentGateway/CathayBank/paymentGateway_CathayBank_yearField'), GlobalVariable.cathayBankCreditCardYear, 
    false)

WebUI.setText(findTestObject('PaymentGateway/CathayBank/paymentGateway_CathayBank_cvvField'), GlobalVariable.cathayBankCreditCardCVV)

'confirm credentials'
WebUI.enhancedClick(findTestObject('PaymentGateway/CathayBank/paymentGateway_CathayBank_confirmBtn'))

'check thank you page heading visible'
WebUI.verifyTextPresent(GlobalVariable.thankYouPageHeading, false)

GlobalVariable.pt_creditCard = WebUI.getText(findTestObject('ThankYouPage/thankYouPage_orderNumber'))


import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.util.concurrent.ThreadLocalRandom as Keyword

'Generate random number'
int randomNumber = Math.abs(new Random().nextInt() % (99999999 - 11111111)) + 11111111

WebUI.openBrowser('')

'Go to WTCTW UAT'
WebUI.navigateToUrl(GlobalVariable.defaultURL)

'Go to my account'
WebUI.click(findTestObject('Home/home_MyAccountBtn'))

'click register button'
WebUI.click(findTestObject('Login/login_RegisterButton'))

'click submit button to trigger errors'
WebUI.click(findTestObject('Registration/registration_SubmitButton'))

'Check error messages'
WebUI.verifyElementText(findTestObject('Registration/errors_registration_Name'), GlobalVariable.globalError_ManFieldMissing)

WebUI.verifyElementText(findTestObject('Registration/errors_registration_Email'), GlobalVariable.globalError_ManFieldMissing)

WebUI.verifyElementText(findTestObject('Registration/errors_registration_EmailVer'), GlobalVariable.globalError_ManFieldMissing)

WebUI.verifyElementText(findTestObject('Registration/errors_registration_Mobile'), GlobalVariable.globalError_ManFieldMissing)

WebUI.verifyElementText(findTestObject('Registration/errors_Registration_Pw'), GlobalVariable.globalError_ManFieldMissing)

WebUI.verifyElementText(findTestObject('Registration/errors_Registration_PwVer'), GlobalVariable.globalError_ManFieldMissing)

'Enter name with randomly generated number'
WebUI.setText(findTestObject('Registration/registration_NameField'), 'Auto Test' + String.valueOf(
        randomNumber))

WebUI.setText(findTestObject('Registration/registraion_EmailField'), 'wrongemail')

WebUI.setText(findTestObject('Registration/registration_EmailVerificationField'), 'wrongwrongemail')

WebUI.setText(findTestObject('Registration/registration_MobileField'), 'wrongmobile')

WebUI.setText(findTestObject('Registration/registration_PasswordField'), 'wrongpw')

WebUI.setText(findTestObject('Registration/registration_PwVerificationField'), 'wrongpassword')

'click submit button to trigger further errors'
WebUI.click(findTestObject('Registration/registration_SubmitButton'))

'check further error messages'
WebUI.verifyElementText(findTestObject('Registration/errors_Registration_EmailFormat'), GlobalVariable.globalError_WrongEmailFormat)

WebUI.verifyElementText(findTestObject('Registration/errors_Registration_EmailMismatch'), GlobalVariable.globalError_EmailMismatch)

WebUI.verifyElementText(findTestObject('Registration/errors_Registration_MobileFormat'), GlobalVariable.globalError_WrongMobileFormat)

WebUI.verifyElementText(findTestObject('Registration/errors_Registration_PwFormat'), errors_Reg_PwFormat)

'Clear wrong input'
WebUI.clearText(findTestObject('Registration/registraion_EmailField'))

WebUI.clearText(findTestObject('Registration/registration_EmailVerificationField'))

WebUI.clearText(findTestObject('Registration/registration_MobileField'))

WebUI.clearText(findTestObject('Registration/registration_PasswordField'))

WebUI.clearText(findTestObject('Registration/registration_PwVerificationField'))

'fill in email'
WebUI.setText(findTestObject('Registration/registraion_EmailField'), ('autotesting-' + String.valueOf(
        randomNumber)) + '@yopmail.com')

'fill in email again'
WebUI.setText(findTestObject('Registration/registration_EmailVerificationField'), ('autotesting-' + 
    String.valueOf(randomNumber)) + '@yopmail.com')

'Enter TW mobile'
WebUI.setText(findTestObject('Registration/registration_MobileField'), '09' + String.valueOf(randomNumber))

'Enter pw'
WebUI.setText(findTestObject('Registration/registration_PasswordField'), GlobalVariable.defaultPassword)

'Enter pw again'
WebUI.setText(findTestObject('Registration/registration_PwVerificationField'), GlobalVariable.defaultPassword)

WebUI.click(findTestObject('Registration/registration_SubmitButton'))

'enter wrong otp'
WebUI.setText(findTestObject('OTP/otp_otpField'), '123456')

WebUI.click(findTestObject('OTP/otp_confirmOTPButton'))

'otp error message'
WebUI.verifyElementText(findTestObject('OTP/errors_OTP_WrongOTP'), GlobalVariable.globalError_WrongOTP)


<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>registration_CountrySelectionDropdown</name>
   <tag></tag>
   <elementGuidId>b38e7162-3c76-43c3-91cb-c49434495eb5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='iMemberForm']/div/div/div[2]/div/button/span[2]/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.yes_card_provide > div.provide > div.width.reset-bootstrp-select > div.btn-group.bootstrap-select > button.btn.dropdown-toggle.btn-default > span.bs-caret > span.caret</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>caret</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;iMemberForm&quot;)/div[@class=&quot;yes_card_provide&quot;]/div[@class=&quot;provide&quot;]/div[@class=&quot;width reset-bootstrp-select&quot;]/div[@class=&quot;btn-group bootstrap-select&quot;]/button[@class=&quot;btn dropdown-toggle btn-default&quot;]/span[@class=&quot;bs-caret&quot;]/span[@class=&quot;caret&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='iMemberForm']/div/div/div[2]/div/button/span[2]/span</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form[3]/div/div/div[2]/div/button/span[2]/span</value>
   </webElementXpaths>
</WebElementEntity>

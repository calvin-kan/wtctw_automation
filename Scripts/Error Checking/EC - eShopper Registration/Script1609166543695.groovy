import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.util.concurrent.ThreadLocalRandom as Keyword

WebUI.openBrowser('')

'Go to WTCTW UAT'
WebUI.navigateToUrl(GlobalVariable.defaultURL)

'Go to my account'
WebUI.click(findTestObject('Home/home_MyAccountBtn'))

'click register button'
WebUI.click(findTestObject('Login/login_RegisterButton'))

WebUI.click(findTestObject('Registration/registration_CountrySelectionDropdown'))

WebUI.click(findTestObject('Registration/registration_CountrySelectionHK'))

'click to trigger errors'
WebUI.click(findTestObject('Registration/registration_SubmitButton'))

WebUI.verifyElementText(findTestObject('Registration/errors_eShopperForm_email'), GlobalVariable.globalError_ManFieldMissing)

WebUI.verifyElementText(findTestObject('Registration/errors_eShopperForm_reEnterEmailAddr'), GlobalVariable.globalError_ManFieldMissing)

WebUI.verifyElementText(findTestObject('Registration/errors_eShopperForm_mobile'), GlobalVariable.globalError_ManFieldMissing)

WebUI.verifyElementText(findTestObject('Registration/errors_eShopperForm_pwd'), GlobalVariable.globalError_ManFieldMissing)

WebUI.verifyElementText(findTestObject('Registration/errors_eShopperForm_checkPwd'), GlobalVariable.globalError_ManFieldMissing)

WebUI.sendKeys(findTestObject('Registration/registration_eShopperEmailField'), 'wrongemail')

WebUI.sendKeys(findTestObject('Registration/registration_eShopperEmailVerField'), 'wrongwrongemail')

WebUI.sendKeys(findTestObject('Registration/registration_eShopperMobileField'), 'wrongmobile')

WebUI.sendKeys(findTestObject('Registration/registration_eShopperPwField'), 'wrongpw')

WebUI.sendKeys(findTestObject('Registration/registration_eShopperPwVerField'), 'wrongpassword')

'Click to trigger further errors'
WebUI.click(findTestObject('Registration/registration_SubmitButton'))

WebUI.verifyElementText(findTestObject('Registration/errors_Registration_eShopperEmailFormat'), GlobalVariable.globalError_WrongEmailFormat)

WebUI.verifyElementText(findTestObject('Registration/errors_Registration_eShopperEmailMismatch'), GlobalVariable.globalError_EmailMismatch)

WebUI.verifyElementText(findTestObject('Registration/errors_Registration_eShopperMobileFormat'), GlobalVariable.globalError_WrongMobileFormat)

WebUI.verifyElementText(findTestObject('Registration/errors_Registration_eShopperPwFormat'), errors_Reg_PwFormat)


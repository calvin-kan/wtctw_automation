import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.codehaus.groovy.runtime.InvokerHelper
import com.kms.katalon.core.logging.KeywordLogger

KeywordLogger logger = new KeywordLogger()
logger.logInfo('HD - Credit Card: '+GlobalVariable.pt_creditCard)
logger.logInfo('HD - Union Pay: '+GlobalVariable.pt_unionPay)
logger.logInfo('HD - NCCC: '+GlobalVariable.pt_ncccInstalment)
logger.logInfo('CC - CCS: '+GlobalVariable.pt_ccs)
logger.logInfo('CC - CCE: '+GlobalVariable.pt_cce)
logger.logInfo('CVS: '+GlobalVariable.pt_cvs)
logger.logInfo('CVS: '+GlobalVariable.pt_cvs_pickpay)
logger.logInfo('Xborder HD: '+GlobalVariable.pt_xb_hd)
logger.logInfo('Xborder SF: '+GlobalVariable.pt_xb_sf)
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.util.concurrent.ThreadLocalRandom as Keyword

'Generate random number'
String randomNumber = String.valueOf(Math.abs(new Random().nextInt() % (9999999 - 1111111)) + 1111111)

'Declare strings'
String eShopperEmail = 'autotesting-' + randomNumber + GlobalVariable.BU + '@yopmail.com'
String eShopperMobile = '9' + randomNumber

WebUI.openBrowser('')

'Go to WTCTW UAT'
WebUI.navigateToUrl(GlobalVariable.defaultURL)

'Go to my account'
WebUI.click(findTestObject('Home/home_MyAccountBtn'))

'click register button'
WebUI.click(findTestObject('Login/login_RegisterButton'))

'Open dropdown'
WebUI.click(findTestObject('Registration/registration_CountrySelectionDropdown'))

'Select HK'
WebUI.click(findTestObject('Registration/registration_CountrySelectionHK'))

'Paste email'
WebUI.sendKeys(findTestObject('Registration/registration_eShopperEmailField'), eShopperEmail)

'Paste email again'
WebUI.sendKeys(findTestObject('Registration/registration_eShopperEmailVerField'), eShopperEmail)

'enter random mobile no.'
WebUI.sendKeys(findTestObject('Registration/registration_eShopperMobileField'), eShopperMobile)

'Enter pw'
WebUI.sendKeys(findTestObject('Registration/registration_eShopperPwField'), GlobalVariable.defaultPassword)

'Enter pw again'
WebUI.sendKeys(findTestObject('Registration/registration_eShopperPwVerField'), GlobalVariable.defaultPassword)

'submit'
WebUI.click(findTestObject('Registration/registration_SubmitButton'))

'Print email and pw to log'
println('\n\n####\nEmail : '+ eShopperEmail + '\nPassword : ' + GlobalVariable.defaultPassword + '\n####')

'check registration successful'
WebUI.verifyTextPresent(GlobalVariable.reg_Success_Welcome, false)

WebUI.verifyTextPresent(GlobalVariable.reg_Success_eShopperText, false)

